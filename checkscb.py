#!/usr/bin/python3

# Copyright 2019 Peter Green
# Released under the MIT/Expat license,

import urllib.request
import lzma
import subprocess
import yaml
import sys
import argparse
from collections import defaultdict
from debian import deb822
import datetime
from email.utils import parsedate_to_datetime
import os

archlist = ['amd64','arm64', 'armel','armhf','i386','mips64el','ppc64el','s390x']
archset = set(archlist)
#dist = 'bullseye'
baseurl = 'https://mirror.mythic-beasts.com/debian'

parser = argparse.ArgumentParser(description="checks debian for self-contained buildability problems")
parser.add_argument("dist",help="distribution to check")
parser.add_argument("--nodownload", help="skip download step, use existing downloaded files", action="store_true")
parser.add_argument("--nodose", help="skip first dose step, use existing dose output files", action="store_true")
parser.add_argument("--snapshot",help="use snapshot from date (YYYYMMDD format)")
parser.add_argument("--datafile",help="update datafile with results")
args = parser.parse_args()

dist=args.dist

if args.snapshot is not None:
	# we need to turn the date into an actual url, we can't hardcode the time
	# because debian snapshot times vary from day to day.
	year = args.snapshot[0:4]
	month = args.snapshot[4:6]
	day = args.snapshot[6:8]
	with urllib.request.urlopen('https://snapshot.debian.org/archive/debian/?year='+year+'&month='+month) as response:
		data = response.read()
	matchstr = b'<a href="'+args.snapshot.encode('ascii')+b'T'
	print(matchstr)
	for line in data.split(b'\n'):
		#print(line[0:18])

		if line[0:18] == matchstr:
			baseurl = 'https://snapshot.debian.org/archive/debian/'+line[9:25].decode('ascii')
			break


def downloadanddecompress(fileurl,outputpath):
	with urllib.request.urlopen(fileurl) as response:
		rdc = lzma.open(response)
		data = rdc.read()
	f = open(outputpath,'wb')
	f.write(data)
	f.close()

url = baseurl+'/dists/'+dist+'/main/source/Sources.xz'
path = 'Sources'
if not args.nodownload:
	print('downloading '+url)
	downloadanddecompress(url,path)

command = ['bash','-o','pipefail','-c','grep-dctrl -F Extra-Source-Only -v yes Sources > Sourcesnoeso']
print("running: "+repr(command))
result = subprocess.call(command)
if result != 0:
	print('failure while filtering out extrasourceonly')
	sys.exit(1)

failures = defaultdict(dict)
neededsources = defaultdict(set)
for arch in archlist:
	url = baseurl+'/dists/'+dist+'/main/binary-'+arch+'/Packages.xz'
	path = arch+'-Packages'
	if not args.nodownload:
		print('downloading '+url)
		downloadanddecompress(url,path)
	if not args.nodose:
		command = ['dose-builddebcheck','--deb-native-arch='+arch,'-f','-o',arch+'-builddebcheck','--explain',path,'Sourcesnoeso']
		print("running: "+repr(command))
		result = subprocess.call(command)
		if result > 1:
			print('unrecognised exit code from dose-builddebcheck, aborting')
			sys.exit(1)
	f = open(arch+'-builddebcheck','r')
	dco = yaml.safe_load(f)
	dco = dco['report']
	for failure in dco:
		source = failure['package']
		reasons = failure['reasons']
		failures[source][arch] = reasons
	f.close()
	#print(repr(dco)[:80])
	#sys.exit(1)
	
	print('reading and parsing '+path)
	f = open(path,'r')
	for pkgentry in deb822.Packages.iter_paragraphs(f):
		if "Source" in pkgentry:
			source = pkgentry["Source"].split(" ")[0]
		else:
			source = pkgentry["Package"]
		neededsources[source].add(pkgentry['Architecture'])
	f.close()
	
	
	
	
	
#print(repr(neededsources))
indepbroken = set()
from collections import OrderedDict
furtherchecksources = OrderedDict()
for source, neededarches in sorted(neededsources.items()):
	#print(source)
	brokenarches = set(failures[source].keys())
	scbarches = neededarches & brokenarches
	if ("all" in neededarches) and (brokenarches == archset):
		scbarches.add("all")
		indepbroken.add(source)
	if scbarches != set():
		furtherchecksources[source] = neededarches

output = b''
i = 0;
while i < len(furtherchecksources):
    print(type(furtherchecksources))
    checkthistime = list(furtherchecksources.keys())[i:i+100]
    print(type(checkthistime))
    checkthistime = '|'.join(checkthistime)
    command = ['grep-dctrl','-P','-e','^('+checkthistime+')$','Sourcesnoeso']
    print("running: "+repr(command))
    output += subprocess.check_output(command)
    i += 100

f = open('furtherchecksources','wb')
f.write(output)
f.close()

failures = defaultdict(dict)
for arch in archlist:
	path = arch+'-Packages'
	command = ['dose-builddebcheck','--deb-native-arch='+arch,'-f','-o',arch+'-builddebcheckao','--explain','--deb-drop-b-d-indep',path,'furtherchecksources']
	print("running: "+repr(command))
	result = subprocess.call(command)
	if result > 1:
		print('unrecognised exit code from dose-builddebcheck, aborting')
		sys.exit(1)
	f = open(arch+'-builddebcheckao','r')
	dco = yaml.safe_load(f)
	dco = dco['report']
	if dco is not None: #if amazingly there were no failures, dose produces empty yaml here which python interprets as "none"
		for failure in dco:
			source = failure['package']
			reasons = failure['reasons']
			failures[source][arch] = reasons
	f.close()
	#print(repr(dco)[:80])
	#sys.exit(1)
	
if args.datafile is not None:
	url = baseurl+'/dists/'+dist+'/Release'
	with urllib.request.urlopen(url) as response:
		data = response.read();
	newdate = None
	for line in data.split(b'\n'):
		if line[:6] == b'Date: ':
			newdate = parsedate_to_datetime(line[6:].decode('ascii')).date()
			break
	if newdate is None:
		for line in data.split(b'\n'):
			print(line)
		print('no date found in release file')
		sys.exit(1)
	olddate = datetime.date(2000,1,1) # dummy date
	olddatatable = {}
	newdatatable = {}
	comments = {}
	
	if os.path.exists(args.datafile):
		f = open(args.datafile)
		firstline = True
		for line in f:
			if firstline:
				olddate=datetime.datetime.strptime(line.strip(),'%Y-%m-%d').date()
				firstline = False
			else:
				linesplit=line.strip().split(' ')
				source = linesplit[0]
				olddatatable[source] = {}
				for archandage in linesplit[1:]:
					if ':' in archandage and '/' not in archandage:
						#print(repr(archandage))
						arch , age = archandage.split(':')
						age = int(age)
						olddatatable[source][arch] = age
					else:
						#treat as comment text if there is no : or if there is a / (to allow URLS)
						if source in comments:
							comments[source] += ' '+archandage
						else:
							comments[source] = archandage
	dayselapsed = (newdate - olddate).days
	

for source, neededarches in furtherchecksources.items():
	brokenarches = set(failures[source].keys())
	scbarches = neededarches & brokenarches
	if source in indepbroken:
		scbarches.add("all")
	if scbarches != set():
		print(source+': '+', '.join(sorted(scbarches)))
		if args.datafile is not None:
			newdatatable[source] = {}
			for arch in scbarches:
				if (source in olddatatable) and (arch in olddatatable[source]):
					newdatatable[source][arch] = olddatatable[source][arch] + dayselapsed
				else:
					newdatatable[source][arch] = 0

if args.datafile is not None:
	f = open(args.datafile,'w')
	f.write(str(newdate)+'\n')
	mipslines = []
	for source, arches in sorted(newdatatable.items(),reverse=True,key = lambda item: sorted(item[1].values(),reverse=True)):
		s = source
		for arch, age in sorted(arches.items()):
			s += " "+arch+':'+str(age)
		if source in comments:
			s += " "+comments[source]
		if (len(arches) == 1) and ("mips64el" in arches):
			mipslines.append(s)
		else:
			f.write(s+'\n')
	for s in mipslines:
		f.write(s+'\n')
	f.close()
