#!/usr/bin/python3

# Copyright 2019 Peter Green
# Released under the MIT/Expat license,

import urllib.request
import lzma
import subprocess
import yaml
import sys
import argparse
from collections import defaultdict
from debian import deb822
import datetime
from email.utils import parsedate_to_datetime
import os

#archlist = ['amd64','arm64', 'armel','armhf','i386','mips64el','mipsel','ppc64el','s390x']
#archset = set(archlist)
#dist = 'bullseye'
baseurl = 'https://mirror.mythic-beasts.com/debian'

parser = argparse.ArgumentParser(description="checks debian for self-contained buildability problems")
#parser.add_argument("dist",help="distribution to check")
parser.add_argument("--nodownload", help="skip download step, use existing downloaded files", action="store_true")
#parser.add_argument("--nodose", help="skip first dose step, use existing dose output files", action="store_true")
parser.add_argument("--snapshot",help="use snapshot from date (YYYYMMDD format)")
parser.add_argument("--datafile",help="update datafile with results")
parser.add_argument("--ubuntu",help="operate on ubuntu")
parser.add_argument("--experimental",help="operate on experimental", action='store_true')

args = parser.parse_args()

#dist=args.dist

if args.snapshot is not None:
	# we need to turn the date into an actual url, we can't hardcode the time
	# because debian snapshot times vary from day to day.
	year = args.snapshot[0:4]
	month = args.snapshot[4:6]
	day = args.snapshot[6:8]
	with urllib.request.urlopen('https://snapshot.debian.org/archive/debian/?year='+year+'&month='+month) as response:
		data = response.read()
	matchstr = b'<a href="'+args.snapshot.encode('ascii')+b'T'
	print(matchstr)
	for line in data.split(b'\n'):
		#print(line[0:18])

		if line[0:18] == matchstr:
			baseurl = 'https://snapshot.debian.org/archive/debian/'+line[9:25].decode('ascii')
			break


def downloadanddecompress(fileurl,outputpath,append = False):
	with urllib.request.urlopen(fileurl) as response:
		rdc = lzma.open(response)
		data = rdc.read()
	if append:
		f = open(outputpath,'ab')
	else:
		f = open(outputpath,'wb')
	f.write(data)
	f.close()

if args.ubuntu is not None:
	baseurl = 'http://archive.ubuntu.com/ubuntu'


semvers = { }
for dist in "testing","unstable":
	semvers[dist] = { }
	url2 = None
	if args.ubuntu is None:
		if args.experimental:
			if dist == "testing":
				realdist = "unstable"
			if dist == "unstable":
				realdist = "experimental"
				semvers["unstable"] = semvers["testing"].copy()
		else:
			realdist = dist;
		url = baseurl+'/dists/'+realdist+'/main/source/Sources.xz'
	elif dist == "testing":
		url = baseurl+'/dists/'+args.ubuntu+'/main/source/Sources.xz'
		url2 = baseurl+'/dists/'+args.ubuntu+'/universe/source/Sources.xz'
	elif dist == "unstable":
		url = baseurl+'/dists/'+args.ubuntu+'-proposed/main/source/Sources.xz'
		url2 = baseurl+'/dists/'+args.ubuntu+'-proposed/universe/source/Sources.xz'
		semvers["unstable"] = semvers["testing"].copy()
	path = 'Sources'+dist
	if not args.nodownload:
		print('downloading '+url)
		downloadanddecompress(url,path)
		if url2 is not None:
			print('downloading '+url2)
			downloadanddecompress(url2,path,True)
	command = ['bash','-o','pipefail','-c','grep-dctrl -F Extra-Source-Only -v yes Sources'+dist+' > Sourcesnoeso'+dist]
	print("running: "+repr(command))
	result = subprocess.call(command)
	if result != 0:
		print('failure while filtering out extrasourceonly')
		sys.exit(1)

	path ="Sourcesnoeso"+ dist
	print('reading and parsing '+path)
	f = open(path,'r')
	for pkgentry in deb822.Sources.iter_paragraphs(f):
		source = pkgentry["Package"]
		if source.startswith("rust-"):
			version = pkgentry["Version"]
			versionsplit = version.split('.')
			#print(source+" "+version)
			major = int(versionsplit[0])
			minor = versionsplit[1] # don't convert "minor" to a number, because some packages have versions like 0.0~git...
			if major > 0:
				semver = str(major)
			else:
				semver = str(major)+'.'+minor
			semvers[dist][source] = semver

for source, semvertesting in semvers['testing'].items():
	if source in semvers['unstable']:
		semverunstable = semvers['unstable'][source]
	else:
		semverunstable = "missing"
	if semvertesting != semverunstable:
		print(source+' '+semvertesting+'->'+semverunstable)

if args.datafile is not None:
	if args.ubuntu:
		url = baseurl+'/dists/'+args.ubuntu+'-proposed/Release'
	else:
		url = baseurl+'/dists/unstable/Release'
	with urllib.request.urlopen(url) as response:
		data = response.read();
	newdate = None
	for line in data.split(b'\n'):
		if line[:6] == b'Date: ':
			newdate = parsedate_to_datetime(line[6:].decode('ascii')).date()
			break
	if newdate is None:
		for line in data.split(b'\n'):
			print(line)
		print('no date found in release file')
		sys.exit(1)
	olddate = datetime.date(2000,1,1) # dummy date
	olddatatable = {}
	newdatatable = {}
	comments = {}
	
	if os.path.exists(args.datafile):
		f = open(args.datafile)
		firstline = True
		for line in f:
			if firstline:
				olddate=datetime.datetime.strptime(line.strip(),'%Y-%m-%d').date()
				firstline = False
			else:
				linesplit=line.strip().split(' ')
				source = linesplit[0]
				semverdiff = linesplit[1]
				age = int(linesplit[2])
				if len(linesplit) > 3:
					comments[source] = ' '.join(linesplit[3:])
				
				olddatatable[source] = (semverdiff,age)
				
		f.close()
	dayselapsed = (newdate - olddate).days
	

	f = open(args.datafile,'w')
	f.write(str(newdate)+'\n')
	
	#print(repr(olddatatable))
	newdatatable = {}
	for source, semvertesting in semvers['testing'].items():
		if source in semvers['unstable']:
			semverunstable = semvers['unstable'][source]
		else:
			semverunstable = "missing"
		if semvertesting != semverunstable:
			semverdiff = semvertesting+'->'+semverunstable
			age = 0
			if source in olddatatable:
				age = olddatatable[source][1] + dayselapsed
			newdatatable[source] = (semverdiff,age)

	
	for source, (semverdiff,age) in sorted(newdatatable.items(),reverse=True,key = lambda item: item[1][1]):
		f.write(source+' '+semverdiff+' '+str(age))
		if source in comments:
			f.write(" "+comments[source])
		f.write('\n')
	f.close()
